D=docker run $$P --rm -it -w$$PWD -v$$PWD:$$PWD --privileged
call:: clean build all
all::
	$D bottletw
build::
	for f in .dockerfiles/*; do docker build $$P -t `basename $$f` $$f; done
	$D tailwind
clean::
	rm -fr output.css __pycache__ ?
	find . -name \*~ -o -name .\*~ | xargs rm -fr
	tree -aI .git
