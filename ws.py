from bottle import *; app = default_app()
import os, sys, gevent

def tailwind():
    cmd = 'tailwindcss -mwo output.css 1>a 2>b'
    assert 0 == os.system(cmd)

if '-w' in sys.argv: gevent.spawn(tailwind)

globals = {
    "title": "The title"
}

@get('/')
def _():
    return redirect('/index.html')

@get('/<path:path>/')
def _(path):
    return redirect(f'/{path}/index.html')

@get('/<path:path>.html')
def _(path):
    return jinja2_template(path, globals)

@get('/<path:path>')
def _(path):
    return static_file(path, 'views')
